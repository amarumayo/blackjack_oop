''' A simple blackjack command line game'''
import random
from time import sleep


class Card:

    """
    A class to represent a card.

    ...

    Attributes
    ----------
    rank : str
        the card rank
    suit : str
        the card suit
    """

    def __init__(self, rank, suit):
        """
        Constructs all the necessary attributes for the card object.

        Parameters
        ----------
            rank : str
                the card rank
            suit : str
                the card suit
        """
        self.rank = rank
        self.suit = suit

    def __repr__(self):
        rep = " of ".join((str(self.rank), self.suit))
        return rep


class Shoe:
    """
    A class to represent a Shoe.
    """

    def __init__(self):
        self.cards = []

    def fill_shoe(self, num_decks=6):
        '''Load a shoe of `num_decks` number of decks'''
        suits = ["Spades", "Clubs", "Hearts", "Diamonds"]
        ranks = ['A', '2', '3', '4', '5', '6',
                 '7', '8', '9', '10', 'J', 'Q', 'K']
        self.cards = [Card(r, s) for r in ranks for s in suits]
        self.cards = self.cards * num_decks

    def clear_shoe(self):
        '''Reset a card shoe'''
        self.cards = []

    def shuffle(self):
        '''Shuffle a shoe'''
        if len(self.cards) > 1:
            print("\nshuffling decks...\n")
            sleep(2)
            random.shuffle(self.cards)

    def deal(self):
        '''Deal a card from a shoe'''
        if len(self.cards) < 104:
            print("\nreshuffling decks...\n")
            self.clear_shoe()
            self.fill_shoe()
            self.shuffle()

        return self.cards.pop(0)


class Hand:
    """
    A class to represent a Hand.
    """

    def __init__(self, is_dealer=False):
        self.dealer = is_dealer
        self.cards = []
        self.win_tally = 0

    def add_card(self, card):
        '''Add a card to a hand instance'''
        self.cards.append(card)

    def reset_hand(self):
        '''Clear cards from a hand instance'''
        self.cards = []

    @property
    def value(self):
        '''Calculate the value of the cards in a hand instance'''
        val = 0
        num_aces = 0

        for card in self.cards:
            if card.rank.isnumeric():
                val += int(card.rank)
            elif card.rank == "A":
                num_aces += 1
                val += 11
            else:
                val += 10
        while num_aces > 0 and val > 21:
            val -= 10
            num_aces -= 1

        return val

    @ property
    def has_blackjack(self):
        '''Determing if a hand instance has a blackjack'''

        blackjack = False

        if len(self.cards) == 2 and self.value == 21:
            blackjack = True

        return blackjack

    @ property
    def is_bust(self):
        '''Determing if a hand instance has bust'''
        bust = False

        if self.value > 21:
            bust = True

        return bust

    def display_hand(self, dealer_reveal=False):
        '''Show the cards in a hand to the screen. Hide dealer first card by default'''

        if self.dealer:

            if not dealer_reveal:
                print('dealer hand (?)')
                print("X", end="\t")

                for card in self.cards[1:]:
                    print(card, end="\t")

            else:
                print(f'dealer hand ({self.value})')
                for card in self.cards:
                    print(card, end="\t")

        else:
            print(f'player hand ({self.value})')
            for card in self.cards:
                print(card, end="\t")

        print('\n')


class Game:
    '''A class to represent a Hand.'''

    def __init__(self):
        self.hands = []
        self.shoe = []
        self.player_turn = True

    def hand_win(self, hand):
        '''Tally a hand as won'''
        if hand.dealer:
            print("dealer wins!")
            hand.win_tally += 1
        else:
            print("player wins!")
            hand.win_tally += 1

    def hit(self, hand):
        '''Hit adds a card from a shoe instance to a hand instance'''
        card = self.shoe.deal()

        # some grammar stuff. Probably a better way to do this
        if hand.dealer:
            who = "Dealer"
        else:
            who = "Player"

        if card.rank in ['A', '8']:
            pronoun = "an"
        else:
            pronoun = "a"

        print(f"{who} recieves {pronoun} {card}.\n")
        hand.add_card(card)
        hand.display_hand(dealer_reveal=True)
        self.check_bust(hand)

    def check_bust(self, hand):
        '''Check if a hand is bust'''
        if hand.is_bust:
            if hand.dealer:
                print("Dealer busts!")
            else:
                self.player_turn = False
                print("Player busts!")

    def player_choice(self, hand):
        '''Prompt player to hit or stand with a hand instance'''

        answer = ''
        while answer not in ['h', 's']:
            answer = input("Hit or Stand? H/S: ")
            if answer.lower() == "h":
                self.hit(hand)
            if answer.lower() == "s":
                print(f"Player stands with hand of {str(hand.value)}\n")
                self.player_turn = False

    def determine_winner(self):
        '''Determine which hand has won'''

        player, dealer = self.hands

        if player.has_blackjack + dealer.has_blackjack > 0:
            # deal with the blackjacks
            if player.has_blackjack + dealer.has_blackjack == 2:
                print("Blackjack Push.")
            elif player.has_blackjack:
                print("Player has blackjack!!!")
                self.hand_win(player)
            else:
                print("Dealer has blackjack!!!")
                self.hand_win(dealer)

        else:
            # deal with other scenarios

            if player.is_bust:
                self.hand_win(dealer)
            elif dealer.is_bust:
                self.hand_win(player)
            else:
                if player.value > dealer.value:
                    self.hand_win(player)
                elif player.value < dealer.value:
                    self.hand_win(dealer)
                else:
                    print("Push.")

    def display_tally(self):
        '''Display the running win tally'''
        player_tally = self.hands[0].win_tally
        dealer_tally = self.hands[1].win_tally
        print(f"The score is player: {player_tally} to dealer: {dealer_tally}")

    def play(self):
        '''play the game'''
        self.shoe = Shoe()
        player = Hand()
        dealer = Hand(is_dealer=True)
        self.hands = [player, dealer]
        self.shoe.fill_shoe()
        self.shoe.shuffle()

        playing = True
        while playing:

            # deal cards
            for i in range(2):   # Avram pylance displays this at not accessed. Is there a better way to loop through something x number of times?
                for hand in self.hands:
                    card = self.shoe.deal()
                    hand.add_card(card)

            # show hands
            dealer.display_hand()
            print("\n")
            player.display_hand()

            if player.has_blackjack or dealer.has_blackjack:
                self.player_turn = False

            # player turn
            while self.player_turn:
                self.player_choice(player)

            # dealer turn if player is not busted
            if not player.is_bust and not \
                    player.has_blackjack and not \
                    dealer.has_blackjack:

                dealer.display_hand(dealer_reveal=True)
                while dealer.value < 17:
                    print("dealer hits")
                    sleep(1)
                    self.hit(dealer)

            self.determine_winner()
            self.display_tally()

            answer = ''
            while answer not in ['y', 'n']:
                answer = input("Play again? Y/N: ")
                if answer.lower() == "n":
                    playing = False
                else:
                    for hand in self.hands:
                        hand.reset_hand()
                        self.player_turn = True

        print("Goodbye")


blackjack = Game()
blackjack.play()
